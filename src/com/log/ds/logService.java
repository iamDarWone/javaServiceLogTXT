package com.log.ds;

import org.apache.log4j.Logger;

import com.konylabs.middleware.common.JavaService2;
import com.konylabs.middleware.controller.DataControllerRequest;
import com.konylabs.middleware.controller.DataControllerResponse;
import com.konylabs.middleware.dataobject.Param;
import com.konylabs.middleware.dataobject.Result;

public class logService implements JavaService2 {
	
	private static final Logger LOG = Logger.getLogger(logService.class);
	public boolean Filerror = true;
	public String idFile;	
	public String rutaServidor;	//
	public String data;	//
	
	@Override
	public Object invoke(String methodID, Object[] objectArray,
			DataControllerRequest request, DataControllerResponse response)
			throws Exception {
		// TODO Auto-generated method stub
		LOG.info("Process Generar file from Java Service Begins - 1");
		LOG.debug("Process Generar file from Java Service Begins - 1");
		
		
		  Result result = new Result();
		  idFile = request.getParameter("idFile");
		  LOG.info("idFile Value is: " +idFile);
		  LOG.debug("idFile Value is: " +idFile);
		  
		  data = request.getParameter("data");
		  LOG.info("data Value is: " +data);
		  LOG.debug("data Value is: " +data);
		
		  rutaServidor =  request.getParameter("rutaServidor");
		  LOG.info("rutaServidor Value is: " +rutaServidor);
		  LOG.debug("rutaServidor Value is: " +rutaServidor);
		  ////////////////////F I L E ///////////////////////
		  Filerror = LogKony.beginFile(rutaServidor,idFile, data); 
		  
		  ////////////////////FIN F I L E ///////////////////
		  if(Filerror){

			  result.setParam(new Param("FILE", "0","string"));
			  result.setParam(new Param("msg", "se escribio en el Archivo correctamente","string"));
			  result.setParam(new Param("opstatus","0","int"));
			  
		  }else{
			  
			  result.setParam(new Param("FILE", "-1","string"));
			  result.setParam(new Param("msg", "Ha ocurrido un problema, Archivo no generado","string"));
			  result.setParam(new Param("opstatus","1","int"));
		  }
		  
		 LOG.info("result object is" + result);  
		 return result;
	}
	
	

}
